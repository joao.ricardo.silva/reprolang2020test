import datetime
import gzip
import os

import numpy as np
import tensorflow as tf

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # disable some excessive console output
np.set_printoptions(precision=4, linewidth=200)


def extract_data_from_mnist_file(mnist_filename):
  """Helper function to read MNIST image files."""

  def _read32(bytestream):
    dt = np.dtype(np.uint32).newbyteorder('>')
    return np.frombuffer(bytestream.read(4), dtype=dt)[0]

  with gzip.open(mnist_filename, 'rb') as bytestream:
    magic = _read32(bytestream)
    if magic == 2049:  # image file
      num_items = _read32(bytestream)
      buffer = bytestream.read(num_items)
      labels = np.frombuffer(buffer, dtype=np.uint8)
      result = labels
    elif magic == 2051:  # label file
      num_images = _read32(bytestream)
      rows = _read32(bytestream)
      cols = _read32(bytestream)
      buffer = bytestream.read(rows * cols * num_images)
      data = np.frombuffer(buffer, dtype=np.uint8)
      result = data.reshape(num_images, rows * cols)
      result = np.multiply(result.astype(np.float32), 1.0 / 255.0)  # rescale
    else:
      raise ValueError('Invalid magic number in MNIST file.')
    return result


# Construction of the computation graph.
# Graph from "Hands-On Machine Learning with Scikit-Learn & Tensorflow".

n_inputs = 28 * 28  # MNIST images are 28 x 28 (= 784).
n_hidden1 = 300  # First hidden layer.
n_hidden2 = 100  # Second hidden layer.
n_outputs = 10  # Output size (one per digit).
learning_rate = 0.01

with tf.name_scope('input'):
  X = tf.placeholder(tf.float32, shape=(None, n_inputs), name='X')
  y = tf.placeholder(tf.int64, shape=(None), name='y')

with tf.name_scope('dnn'):
  hidden1 = tf.layers.dense(X, n_hidden1, activation=tf.nn.relu, name='hidden1')
  hidden2 = tf.layers.dense(hidden1, n_hidden2, activation=tf.nn.relu, name='hidden2')
  logits = tf.layers.dense(hidden2, n_outputs, name='outputs')

with tf.name_scope('loss'):
  # Calculating cross-entropy directly can be numerically unstable.
  xentropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y, logits=logits)
  loss = tf.reduce_mean(xentropy, name='loss')

with tf.name_scope('train'):
  optimizer = tf.train.GradientDescentOptimizer(learning_rate)
  training_op = optimizer.minimize(loss)

with tf.name_scope('eval'):
  correct = tf.nn.in_top_k(logits, y, k=1)
  accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))

init = tf.global_variables_initializer()


# Execution phase.
# Warning: "Feeding" is the least efficient way to feed data into a TensorFlow 
# program and should only be used for small experiments and debugging.
# (from https://www.tensorflow.org/api_guides/python/reading_data)

n_epochs = 1
batch_size = 50
validation_size = 5000

# Paths and filenames for datasets.
input_dir = os.environ['INPUT_DIR']
run_test_dir = os.environ['INPUT_TEST_DIR']
comparables_dir = os.environ['COMPARABLES']
filename_train_images = os.path.join(input_dir, 'train-images-idx3-ubyte.gz')
filename_train_labels = os.path.join(input_dir, 'train-labels-idx1-ubyte.gz')
filename_test_images = os.path.join(run_test_dir, 't10k-images-idx3-ubyte.gz')
filename_test_labels = os.path.join(run_test_dir, 't10k-labels-idx1-ubyte.gz')

# Read and batch datasets.
images = extract_data_from_mnist_file(filename_train_images)
images_validation, images_train = images[:validation_size], images[validation_size:]
labels = extract_data_from_mnist_file(filename_train_labels)
labels_validation, labels_train = labels[:validation_size], labels[validation_size:]
dataset_train = tf.data.Dataset.from_tensor_slices((images_train, labels_train))
dataset_validation = tf.data.Dataset.from_tensor_slices((images_validation, labels_validation))
batched_dataset_train = dataset_train.batch(batch_size)
batched_dataset_validation = dataset_validation.batch(validation_size)

# Create an iterator (which will be used for both the training and the validation datasets).
iterator = tf.data.Iterator.from_structure(batched_dataset_train.output_types, batched_dataset_train.output_shapes)
iter_train_init = iterator.make_initializer(batched_dataset_train)
iter_validation_init = iterator.make_initializer(batched_dataset_validation)
next_element = iterator.get_next()

# Run the computation.
with tf.Session() as sess:
  sess.run(init)

  # Training (and validation)
  for epoch in range(n_epochs):
    print('Epoch {}'.format(epoch))
    sess.run(iter_train_init)
    while True:
      try:
        X_train_batch, y_train_batch = sess.run(next_element)
        sess.run(training_op, feed_dict={X: X_train_batch, y: y_train_batch})
      except tf.errors.OutOfRangeError:
        break
    sess.run(iter_validation_init)
    X_val_batch, y_val_batch = sess.run(next_element)
    acc_val = accuracy.eval(feed_dict={X: X_val_batch, y: y_val_batch})
    print(' validation accuracy: {:.2%}'.format(acc_val))

  # Evaluation
  images_test = extract_data_from_mnist_file(filename_test_images)
  labels_test = extract_data_from_mnist_file(filename_test_labels)
  dataset_test = tf.data.Dataset.from_tensor_slices((images_test, labels_test))
  batched_dataset_test = dataset_test.batch(len(labels_test))  # hack
  iterator = batched_dataset_test.make_one_shot_iterator()
  next_element = iterator.get_next()
  while True:
    try:
      X_test, y_test = sess.run(next_element)
      acc_test = accuracy.eval(feed_dict={X: X_test, y: y_test})
      print('Testing accuracy: {:.2%}'.format(acc_test))
    except tf.errors.OutOfRangeError:
      break
  with open(os.path.join(comparables_dir, 'accuracy'), 'w') as fh:
    fh.write('Testing accuracy: {:.2%}\n'.format(acc_test))
